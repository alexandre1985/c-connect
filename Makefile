CFLAGS=-Wall -g

all: clean-linux c-connect

clean:
	find . -maxdepth 1 -type f -executable -delete

clean-linux:
	rm -f c-connect

clean-windows:
	rm -f c-connect.exe

run: all
	gdb c-connect

mem: all
	valgrind --leak-check=full --show-leak-kinds=all -s c-connect

linux: clean-linux
	${CC} -Wall -O2 -o c-connect c-connect.c

windows: clean-windows
	x86_64-w64-mingw32-gcc -o c-connect.exe c-connect.c

build: linux windows
