# Connect Four for Pro Players

This is a connect four game for Linux and Windows. It can be set the:
- number of players
- number of rows and lines of the board
- number of pieces to be connected
- directions in which a win is achieved (horizontal, vertical,
  back-slash diagonal, forward-slash diagonal)

![](https://i.imgur.com/2fXyBQU.png)

This settings are meant to be a fun way of playing connect four game.

## Build
### Linux
```
make linux
```
### Windows
```
make windows
```

## Usage
If you need help use:
```
./c-connect help
```
The arguments defaults are:
```
./c-connect 2 7 6 4 hvfb
```
To play a game with the default options use:
```
./c-connect
```

## Things to implement
1. Show color on last played token(s) by the opponent(s)
2. Ranking system of wins, losses and draws
3. Network/Internet multi-player

## License
GPL-2.0-or-later
