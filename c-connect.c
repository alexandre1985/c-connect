#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/* Get terminal columns */
#ifdef _WIN32
#include <windows.h>
#endif

#ifdef linux
#include <sys/ioctl.h>
#include <unistd.h>
#endif
/* end */

#define MAX_CHARS_PROMPT 10

typedef unsigned short big;

// Game Globals
big boardColumns = 7, boardRows = 6, connectNumber = 4, numberPlayers = 2;
big** board = NULL;
big** moves = NULL;
big** winningPieces = NULL;
big currentPlayer = 1;
bool winHorizontal = false, winVertical = false, winBackDiagonal = false, winFrontDiagonal = false;


void helpAndQuit()
{
	printf("usage: c-connect [number-of-players] [board-columns] [board-rows] [connect-number] [winning-direction]\n\n");
	printf(" > the winning-direction argument may have the following chars in order to represent the winning directions:\n      h - for horizontal win\n      v - for vertical win\n      f - for diagonally forward-slash win\n      b - for diagonally back-slash win.\n");
	printf(" > any argument may have the char '-' to represent the argument default value.\n");
	printf(" > the default argument values are: c-connect 2 7 6 4 hvfb\n");
	exit(0);
}

void clearScreen()
{
	system("@cls||clear");
}

void freeGlobalVariablesMemory()
{
	for(big i = 0; i <= boardRows-1; ++i) {
		free(board[i]);
	}
	free(board);
	for(big i=0; i <= numberPlayers-1 ; ++i) {
		free(moves[i]);
	}
	free(moves);
	for(big i=0; i <= connectNumber-1 ; ++i) {
      free(winningPieces[i]);
   }
   free(winningPieces);
}

void quit()
{
	freeGlobalVariablesMemory();
	exit(0);
}

void quitWithError(big error)
{
	freeGlobalVariablesMemory();
	exit(error);
}

void flush_input()
{
	getchar();
}

big getColumnsTerminalWindow()
{
	//big rows;
	big columns;
#ifdef linux
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	//rows = w.ws_row;
	columns = w.ws_col;
#endif

#ifdef _WIN32
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	//rows = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
	columns = csbi.srWindow.Right - csbi.srWindow.Left + 1;
#endif

	return columns;
}

char getPlayerCoin(big player)
{
	return (player < 10) ? '0'+player : 'A'+(player-10);
	
}

big nextPlayer(big player)
{
	return (player > numberPlayers) ? 1 : ++player;
}

void printBoardChar(char character)
{
	printf("\033[1;34m%c\033[m", character);
}

void printColumnNumber(big number)
{
	printf("\033[1;35m%d\033[m", number);
}

void printCoin(big player, bool colored)
{
	if(colored) {
		// printing in green
		printf("\033[1;32m%c\033[m", getPlayerCoin(player));
	} else { // if it is not colored
		printf("%c", getPlayerCoin(player));
	}
}

bool isAColorPlace(big row, big column)
{
	for(big i=1; i <= connectNumber; ++i) {
		if(row == winningPieces[i-1][0] && column == winningPieces[i-1][1])
			return true;
	}
	return false;
}

void drawIntoBoard(big turnColumn, bool colored)
{
	/* MOVE INTO BOARD VARIABLE */

	/* draw piece in column
	 * if board place of column is empty, fill it
	 * if board place of column is full, try the above place
	 */
	// if first draw, skip; else set board token	
	if(turnColumn != 0) {
		for(big row=boardRows-1; !(row == 0 && board[row][turnColumn-1] != 0); --row)
		{
			if(board[row][turnColumn-1] == 0) {
				// chosen the token as the player number
				board[row][turnColumn-1] = currentPlayer;
				break;
			}
		}
	}

	/* DRAW BOARD */
	
	clearScreen();
	// check if board will be drawn with spaces for better diagonal view
	big terminalColumns = getColumnsTerminalWindow();
	bool spacedDraw = (boardColumns+3 <= (terminalColumns/2 + terminalColumns%2)) ? true : false;

	big placehold;
	for(big i=0; i < boardRows; ++i) {
		for(big j=0; j < boardColumns; ++j) {
			if(j == 0) {
				printBoardChar('|');
				if(spacedDraw) printf(" ");
			}
			
			placehold = board[i][j];
			if(placehold == 0) {
				printf(" ");
			}
			else {
				if(colored && isAColorPlace(i,j)) {
					printCoin(placehold, true);
				} else {
					printCoin(placehold, false);
				}
			}
			
			if(spacedDraw) printf(" ");
			if(j == boardColumns-1) {
				printBoardChar('|');
				printf("\n");
			}
		}
	}
	// draw base of board
	printBoardChar('|');
	if(spacedDraw) printf(" ");
	for(big i=0; i < boardColumns; ++i) {
		printBoardChar('^');
		if(spacedDraw) printf(" ");
	}
	printBoardChar('|');
	printf("\n");
	printBoardChar('|');
	if(spacedDraw) printf(" ");
	for(big i=1; i <= boardColumns; ++i) {
		printBoardChar('|');
		if(spacedDraw) printf(" ");
	}
	printBoardChar('|');
	printf("\n");
	printBoardChar(':');
	if(spacedDraw) printf(" ");
	// draw column numbers
	if(boardColumns >= 10) {
		for(big i=1; i <= boardColumns; ++i) {
			printColumnNumber(i/10);
			if(spacedDraw) printf(" ");
		}

		printBoardChar(':');
		printf("\n");
		printBoardChar(':');
		if(spacedDraw) printf(" ");
	}
	for(big i=1; i <= boardColumns; ++i) {
		printColumnNumber(i%10);
		if(spacedDraw) printf(" ");
	}
	printBoardChar(':');
	printf("\n");
}

big processInputOfColumn(char response[MAX_CHARS_PROMPT])
{
	big column = 0;
	// if is a number
	if(strspn(response, "0123456789") == strlen(response)) {
		column = atoi(response);
		if(column < 1 || column > boardColumns) {
			printf("Choose a column number from 1 until %d\n", boardColumns);
		} else if(board[0][column-1] != 0) {
			printf("The column %d is full. Choose another one.\n", column);
		} else {
			return column;
		}
	} else if(strcmp(response, "quit") == 0 || strcmp(response, "q") == 0) {
		free(response);
		quit();
	} else if(strcmp(response, "help") == 0 || strcmp(response, "h") == 0) {
		printf("Choose a column number from 1 until %d. Use quit for quitting the game.\n", boardColumns);
	} else {
		printf("Choose a column number from 1 until %d\n", boardColumns);
	}

	return 0;
}

bool isWinHorizontal(big rowIndexZero, big columnIndexZero) {
	big piecesInARow = 0;
	// check from placehold that was played to the left
	big rowTemp = rowIndexZero, columnTemp = columnIndexZero;
	do {
		if(board[rowTemp][columnTemp] != currentPlayer) break;
 		++piecesInARow;
		if(columnTemp == 0) break;
		--columnTemp;
	} while(1);
	// check from placehold that was played to the right
	rowTemp = rowIndexZero, columnTemp = columnIndexZero;
	do {
		if(columnTemp == boardColumns-1) break;
		++columnTemp;
		if(board[rowTemp][columnTemp] != currentPlayer) break;
 		++piecesInARow;
	} while(1);
	return (piecesInARow >= connectNumber) ? true : false;
}

bool isWinVertical(big rowIndexZero, big columnIndexZero) {
	big piecesInARow = 0;
	// check from placehold that was played downwards
	big rowTemp = rowIndexZero, columnTemp = columnIndexZero;
	do {
		if(board[rowTemp][columnTemp] != currentPlayer) break;
 		++piecesInARow;
		if(rowTemp == boardRows-1) break;
		++rowTemp;
	} while(1);
	return (piecesInARow >= connectNumber) ? true : false;
}

bool isWinFrontDiagonal(big rowIndexZero, big columnIndexZero) {
	big piecesInARow = 0;
	// check from placehold that was played downwards to the left
	big rowTemp = rowIndexZero, columnTemp = columnIndexZero;
	do {
		if(board[rowTemp][columnTemp] != currentPlayer) break;
 		++piecesInARow;
		if(columnTemp == 0 || rowTemp == boardRows-1 ) break;
		--columnTemp;
		++rowTemp;
	} while(1);
	// check from placehold that was played upwards to the right
	rowTemp = rowIndexZero, columnTemp = columnIndexZero;
	do {
		if(columnTemp == boardColumns-1 || rowTemp == 0) break;
		++columnTemp;
		--rowTemp;
		if(board[rowTemp][columnTemp] != currentPlayer) break;
 		++piecesInARow;
	} while(1);
	return (piecesInARow >= connectNumber) ? true : false;
}

bool isWinBackDiagonal(big rowIndexZero, big columnIndexZero) {
	big piecesInARow = 0;
	// check from placehold that was played upwards to the left
	big rowTemp = rowIndexZero, columnTemp = columnIndexZero;
	do {
		if(board[rowTemp][columnTemp] != currentPlayer) break;
 		++piecesInARow;
		if(columnTemp == 0 || rowTemp == 0 ) break;
		--columnTemp;
		--rowTemp;
	} while(1);
	// check from placehold that was played downwards to the right
	rowTemp = rowIndexZero, columnTemp = columnIndexZero;
	do {
		if(columnTemp == boardColumns-1 || rowTemp == boardRows-1) break;
		++columnTemp;
		++rowTemp;
		if(board[rowTemp][columnTemp] != currentPlayer) break;
 		++piecesInARow;
	} while(1);
	return (piecesInARow >= connectNumber) ? true : false;
}

big isWin(big column)
{
	big row = 0;
	// get board place location
	for(; 1; ++row) {
		if(board[row][column-1] != 0) {
			break;
		}
	}
	if(winHorizontal && isWinHorizontal(row, column-1)) return 1;
	if(winVertical && isWinVertical(row, column-1)) return 2;
	if(winFrontDiagonal && isWinFrontDiagonal(row, column-1)) return 3;
	if(winBackDiagonal && isWinBackDiagonal(row, column-1)) return 4;

	return 0;
}

bool askIsRestartGame()
{
	char* replay;
	do {
		printf("Do you want to play this game again [y/n]? ");
		if(scanf("%ms", &replay) == 1) {
			if(strcmp(replay, "y") == 0 || strcmp(replay, "yes") == 0) {
				flush_input();
				free(replay);
				return true;
			}
			else if(strcmp(replay, "n") == 0 || strcmp(replay, "no") == 0) {
				free(replay);
				quit();
			}
			else {
				flush_input();
				free(replay);
				printf("Press y for yes or n for no.\n");
			}
		} else {
			// hit ^D
			printf("n\n");
			quit();
		}
	} while(1);
}

bool isDraw()
{
	for(big i=1; i <= boardColumns; ++i) {
		if(board[0][i-1] == 0) return false;
	}
	return true;
}

void drawWinner(big column, big winningDirection)
{
	// column and row to index 0
	--column;
	// get board place location
	big row = 0;
	for(; 1; ++row) {
		if(board[row][column] != 0) {
			break;
		}
	}
	// get winningPieces
	big piecesConnected = 0;
	// check from placehold that was played to the left
	big rowTemp = row, columnTemp = column;
	switch(winningDirection) {
		case 4: // diagonal back-slash
			do {
				if(board[rowTemp][columnTemp] != currentPlayer) break;
				winningPieces[piecesConnected][0] = rowTemp;
				winningPieces[piecesConnected++][1] = columnTemp;
				if(columnTemp == 0 || rowTemp == 0 ) break;
				--columnTemp;
				--rowTemp;
			} while(1);
			// check from placehold that was played downwards to the right
			rowTemp = row, columnTemp = column;
			do {
				if(columnTemp == boardColumns-1 || rowTemp == boardRows-1) break;
				++columnTemp;
				++rowTemp;
				if(board[rowTemp][columnTemp] != currentPlayer) break;
				winningPieces[piecesConnected][0] = rowTemp;
				winningPieces[piecesConnected++][1] = columnTemp;
			} while(1);
			break;
		case 3: // diagonal forward-slash
			do {
				if(board[rowTemp][columnTemp] != currentPlayer) break;
				winningPieces[piecesConnected][0] = rowTemp;
				winningPieces[piecesConnected++][1] = columnTemp;
				if(columnTemp == 0 || rowTemp == boardRows-1 ) break;
				--columnTemp;
				++rowTemp;
			} while(1);
			// check from placehold that was played upwards to the right
			rowTemp = row, columnTemp = column;
			do {
				if(columnTemp == boardColumns-1 || rowTemp == 0) break;
				++columnTemp;
				--rowTemp;
				if(board[rowTemp][columnTemp] != currentPlayer) break;
				winningPieces[piecesConnected][0] = rowTemp;
				winningPieces[piecesConnected++][1] = columnTemp;
			} while(1);
			break;
		case 2: // vertical
			// check from placehold that was played downwards
			do {
				if(board[rowTemp][columnTemp] != currentPlayer) break;
				winningPieces[piecesConnected][0] = rowTemp;
				winningPieces[piecesConnected++][1] = columnTemp;
				if(rowTemp == boardRows-1) break;
				++rowTemp;
			} while(1);
			break;
		case 1: // horizontal
			// check from placehold that was played to the right
			do {
				if(board[rowTemp][columnTemp] != currentPlayer) break;
				winningPieces[piecesConnected][0] = rowTemp;
				winningPieces[piecesConnected++][1] = columnTemp;
				if(columnTemp == 0) break;
				--columnTemp;
			} while(1);
			// check from placehold that was played to the right
			rowTemp = row, columnTemp = column;
			do {
				if(columnTemp == boardColumns-1) break;
				++columnTemp;
				if(board[rowTemp][columnTemp] != currentPlayer) break;
				winningPieces[piecesConnected][0] = rowTemp;
				winningPieces[piecesConnected++][1] = columnTemp;
			} while(1);
			break;
		default:
			printf("This is not supposed to happen. An issue at the code repository.\n");
			quitWithError(223);
	}
	// print board
	for(big i=1; i <= connectNumber ; ++i) {
		printf("%d, %d\n", winningPieces[i-1][0], winningPieces[i-1][1]);
	}
	drawIntoBoard(0, true);
}

void play()
{
	// types
	big numberBoardPlaces, maxNumberTurnsPerPlayer, turnColumn, moveNumber;
	char* response;
	// default values;
	numberBoardPlaces = boardColumns * boardRows;
	maxNumberTurnsPerPlayer = (numberBoardPlaces/numberPlayers)+(numberPlayers%2);
	// allocate memory for board and moves
	board = malloc(boardRows * sizeof(big*));
	for(big i=0; i <= boardRows-1 ; ++i) {
		board[i] = malloc(boardColumns * sizeof(big));
	}
	moves = malloc(numberPlayers * sizeof(big*));
	for(big i=0; i <= numberPlayers-1 ; ++i) {
		moves[i] = malloc(maxNumberTurnsPerPlayer * sizeof(big));
	}
	winningPieces = malloc(connectNumber * sizeof(big*));
	for(big i=1; i <= connectNumber; ++i) {
		winningPieces[i-1] = malloc(2 * sizeof(big));
	}

playing:
	turnColumn = 0, moveNumber = 0;
	response = NULL;
	// initialize board and moves with zeros
	for(big i=0; i <= boardRows-1 ; ++i) {
		for(big j=0; j <= boardColumns-1 ; ++j) {
			board[i][j] = 0;
		}
	}
	for(big i=0; i <= numberPlayers-1 ; ++i) {
		for(big j=0; j <= maxNumberTurnsPerPlayer-1 ; ++j) {
			moves[i][j] = 0;
		}
	}
	// first draw
	drawIntoBoard(0, false);
	// play turn each player
	for(big turn = 1; moveNumber <= numberBoardPlaces; ++turn) {
		// reset variable from the loop above
		currentPlayer = 1;
		for(currentPlayer = 1; currentPlayer <= numberPlayers; ++currentPlayer) {
			do {
				printf("player %c, drop in column> ", getPlayerCoin(currentPlayer));
				if(scanf("%ms", &response) == 1) {
					turnColumn = processInputOfColumn(&response[0]);

					flush_input();
					free(response);
				} else {
					// hit of ^D
					printf("q\n");
					quit();
				}
			} while(turnColumn == 0);

			moves[currentPlayer-1][turn-1] = turnColumn;

			drawIntoBoard(turnColumn, false);
			// check if is a draw
			if(isDraw()) {
				printf("This game is a draw.\n");
				if(askIsRestartGame()) goto playing;
			}
			// check if player has won
			big winningDirection = isWin(turnColumn);
			if(winningDirection != 0) {
				char winningDirectionString[23];
				switch(winningDirection) {
					case 4:
						strcpy(winningDirectionString, "diagonal back-slash");
						break;
					case 3:
						strcpy(winningDirectionString, "diagonal forward-slash");
						break;
					case 2:
						strcpy(winningDirectionString, "vertical");
						break;
					case 1:
						strcpy(winningDirectionString, "horizontal");
						break;
					default:
						printf("This is not supposed to happen. An issue at the code repository.\n");
						quitWithError(222);
				}
				// draw with color the winning pieces
				drawWinner(turnColumn, winningDirection);
				// print winner
				printf("Win for player %c in the %s!\n", getPlayerCoin(currentPlayer), winningDirectionString);
				if(askIsRestartGame()) goto playing;
			}
			++moveNumber;
		}
	}
	// putting this just to remove the gcc warning of move not being used
	moves[0][0] = moves[0][0];
}

int main(int argc, char* argv[])
{	
	/* CLI ARGUMENTS */
	
	// capturing arguments from command line	
	if(argc > 1) {
		switch(argc) {
			case 6:
				// down below is the setting as default with a '-' argument
				if(strchr(argv[5], 'h') != NULL) winHorizontal = true;
				if(strchr(argv[5], 'v') != NULL) winVertical = true;
				if(strchr(argv[5], 'f') != NULL) winFrontDiagonal = true;
				if(strchr(argv[5], 'b') != NULL) winBackDiagonal = true;
			case 5:
				if(strcmp(argv[4], "-") != 0) connectNumber = atoi(argv[4]);
			case 4:
				if(strcmp(argv[3], "-") != 0) boardRows = atoi(argv[3]);
			case 3:
				if(strcmp(argv[2], "-") != 0) boardColumns = atoi(argv[2]);
			case 2:
				if(strcmp(argv[1], "help") == 0 || strcmp(argv[1], "h") == 0) {
					helpAndQuit();
				} else {
					if(strcmp(argv[1], "-") != 0) numberPlayers = atoi(argv[1]);
				}
				break;
			default:
				helpAndQuit();
		}
	}
	// checking arguments limits
	if(numberPlayers < 2 || numberPlayers > 20) {
		printf("error: number-of-players must be in range of [2..20] .\n");
		quitWithError(1);
	}
	if(connectNumber < 4 || connectNumber > 9) {
		printf("error: connect-number must be in range of [4..9] .\n");
		quitWithError(1);
	}
	if(boardRows < 6 || boardRows > 35) {
		printf("error: board-rows must be in range of [6..35] .\n");
		quitWithError(1);
	}
	if(boardColumns < 7 || boardColumns > 48) {
		printf("error: board-columns must be in range of [7..48] .\n");
		quitWithError(1);
	}
	// default value of winning direction
	if(argc < 6 || strcmp(argv[5], "-") == 0) {
		winHorizontal = true;
		winVertical = true;
		winFrontDiagonal = true;
		winBackDiagonal = true;
	}

	play();
	
	quit();
}
